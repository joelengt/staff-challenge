from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Product, ProductDetails
from import_export import resources
from import_export import fields
from import_export.admin import ImportExportMixin

class ProductResource(resources.ModelResource):
    id = fields.Field(column_name="ID")
    created = fields.Field(column_name="CREATED")
    modified = fields.Field(column_name="MODIFIED")
    is_active = fields.Field(column_name="IS_ACTIVE")
    type_product = fields.Field(column_name="TYPE")
    name = fields.Field(column_name="NAME")
    description = fields.Field(column_name="DESCRIPTION")
    is_variation = fields.Field(column_name="IS_ACTIVE")
    brand_id = fields.Field(column_name="BRAND_ID")
    code = fields.Field(column_name="CODE")
    family = fields.Field(column_name="FAMILY")
    is_complement = fields.Field(column_name="IS_COMPLEMENT")
    is_delete = fields.Field(column_name="IS_DELETE")



    class Meta:
        model = Product

class ProductDetailsResource(resources.ModelResource):
    id = fields.Field(column_name="ID")
    created = fields.Field(column_name="CREATED")
    modified = fields.Field(column_name="MODIFIED")
    is_active = fields.Field(column_name="IS_ACTIVE")
    is_visibility = fields.Field(column_name="IS_VISIBILITY")
    price = fields.Field(column_name="PRICE")
    price_offer = fields.Field(column_name="PRICE_OFFER")
    offer_day_from = fields.Field(column_name="OFFER_DAY_FROM")
    offer_day_to = fields.Field(column_name="OFFER_DAY_TO")
    quantity = fields.Field(column_name="QUANTITY")
    sku = fields.Field(column_name="SKU")
    product = fields.Field(column_name="PRODUCT_ID")


    class Meta:
        model = ProductDetails


# Register your models here.

 

class ProductDetailsInline(admin.TabularInline):
  extra = 0
  model = ProductDetails

class ProductDetailsAdmin( ImportExportMixin, admin.ModelAdmin):
    resource_class = ProductDetailsResource


class ProductAdmin( ImportExportMixin, admin.ModelAdmin):
    save_as = True
    resource_class = ProductResource
    list_display = ('id', 'created', 'modified', 'is_active', 'type_product', 'name', 'description', 'is_variation', 'brand_id', 'code', 'family','is_complement', 'is_delete',)
    list_filter = ('created','modified','type_product',)
    search_fields = ('name', 'description',)
    ordering = ('created',)
    inlines = [ProductDetailsInline,]

admin.site.register(ProductDetails, ProductDetailsAdmin)
admin.site.register(Product, ProductAdmin)

